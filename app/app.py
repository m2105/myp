from flask import Flask
from config import Configuration


from datetime import datetime
import bcrypt

app = Flask(__name__)
app.secret_key = 'mysecret'
app.config.from_object(Configuration)
