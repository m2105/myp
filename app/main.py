from app import app  # import our Flask app
import views

if __name__ == '__main__':
    app.secret_key = 'mysecret'
    app.run()
