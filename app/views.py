from flask import render_template, Flask, url_for , request, session, redirect
import pymongo
import bcrypt
import uuid
import datetime
from app import app

# TODO
# use save method to update https://www.youtube.com/watch?v=k-NSNUKk1Kw
# or look at : https://docs.mongodb.com/manual/tutorial/update-documents/
# sslify https://github.com/kennethreitz/flask-sslify
# add a field in a document to state if a student is excused and do no count
# Add a way to edit
#   - fiche (for exemple change hours, name, date, time, ..
#   - implies editing the value for all student
#   - edit student attendance, and justification
#   - a way to add a student into a fiche
#   - student profile (change cm, td, tp, etc.)

class Database(object):
    URI = "mongodb://127.0.0.1:27017"
    DATABASE = None

    @staticmethod
    def initialize():
        client = pymongo.MongoClient(Database.URI)
        Database.DATABASE = client['presence']

    @staticmethod
    def insert(collection, data):
        Database.DATABASE[collection].insert(data)

    @staticmethod
    def find(collection, query):
        return Database.DATABASE[collection].find(query)

    @staticmethod
    def find_one(collection, query):
        return Database.DATABASE[collection].find_one(query)

    @staticmethod
    def remove(collection, query):
        return Database.DATABASE[collection].remove(query)


class Fiche(object):
    def __init__ (self,
            name, nom, prenom, promo, statut,
            date, hh, duree, module, groupe, pres, prof,
            fid, excuse
            ):
        self.name = name
        self.nom = nom
        self.prenom = prenom
        self.promo = promo
        self.statut = statut

        self.date = date
        self.hh = hh
        self.duree = duree
        self.module = module
        self.groupe = groupe
        self.pres = pres
        self.prof = prof

        self.fid = fid
        self.excuse = excuse
        self.id = uuid.uuid4().hex

    def save_to_mongo(self):
        Database.insert(collection='fiche', data=self.json())

    def json(self):
        return {
            'name' : self.name,
            'nom' : self.nom,
            'prenom' : self.prenom,
            'promo' : self.promo,
            'statut' : self.statut,

            'date' : self.date,
            'hh' : self.hh,
            'duree' : self.duree,
            'module' : self.module,
            'groupe' : self.groupe,
            'pres' : self.pres,
            'prof' : self.prof,

            'fid' : self.fid,
            'excuse' : self.excuse,
            'id' : self.id
        }


class User(object):
    def __init__ (self, name, password, nom, prenom, tp, td, cm, promo, statut):
        self.name = name
        self.nom = nom
        self.prenom = prenom
        self.tp = tp
        self.td = td
        self.cm = cm
        self.promo = promo
        self.statut = statut
        self.password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

    def save_to_mongo(self):
        Database.insert(collection='users', data=self.json())

    def json(self):
        return {
            'name' : self.name,
            'nom' : self.nom,
            'prenom' : self.prenom,
            'tp' : self.tp,
            'td' : self.td,
            'cm' : self.cm,
            'promo' : self.promo,
            'statut' : self.statut,
            'password' : self.password
        }

    @staticmethod
    def from_mongo(user):
        return Database.find_one(collection='users', query={'name' : user})

Database.initialize()

@app.route('/')
def index():
    session.clear()
    return render_template('index.html')

@app.route('/login', methods=['POST'])
def login():
    u = request.form['username']
    existing_user = Database.find_one(collection='users', query={'name' : u })

    if existing_user:
        p = bcrypt.hashpw(request.form['pass'].encode('utf-8'), existing_user['password'])
        if p == existing_user['password']:
            session['username'] = request.form['username']
            session['statut'] = existing_user['statut']
            return redirect(url_for('menu'))
        return 'Wrong username or password !!'
    return 'Wrong username or password !!'

@app.route('/menu')
def menu():
    if 'username' not in session:
        return redirect(url_for('index'))
    if session['statut'] == 'ens':
        return render_template('ens00.html', s=session)

    return 'Hello ' + session['username'] + '/' + session['statut']

@app.route('/ens/stats', methods=['POST', 'GET'])
def stats():
    #h, m = divmod(180,60)
    if 'username' not in session:
        return redirect(url_for('index'))
    if request.method == 'GET':
        if session['statut'] == 'ens':
            fiche = Database.find(collection='fiche',
                query={
                    'statut' : session['statut'],
                    'name' : session['username']
                    })
            return render_template('show_liste.html', f=fiche, s=session )

    return 'show real stats'

@app.route('/ens/edit', methods=['POST', 'GET'])
def edit():
    if 'username' not in session:
        return redirect(url_for('index'))
    if request.method == 'GET':
        if session['statut'] == 'ens':
            return redirect(url_for('stats'))

    if request.form['action'] == 'edit':
        fiche = Database.find(collection='fiche',
            query={ 'fid' : request.form['fid'] ,
                    'statut' : 'etu'
                })
        return render_template('show_fiche.html', f=fiche, s=session )

    if request.form['action'] == 'delete':
        fiche = Database.remove(collection='fiche',
            query={ 'fid' : request.form['fid']
                })
        return redirect(url_for('edit'))

    if request.form['action'] == 'update':
        fiche = Database.find(collection='fiche',
            query={ 'fid' : request.form['fid']
                })
        for f in fiche :
            if f['duree'] < int(request.form['duree']):
                # mon absence est de:
                d =  int(request.form['duree']) - (f['duree'] - f['pres'])
            else :
                d = (f['pres']* int(request.form['duree'])) / f['duree']
            f['pres'] = int(d)
            f['module'] = request.form['module']
            f['groupe'] = request.form['groupe']
            f['date'] = request.form['date']
            f['hh'] = request.form['hh']
            f['duree'] = int(request.form['duree'])
            new_fiche = Fiche(
                f['name'],
                f['nom'],
                f['prenom'],
                f['promo'],
                f['statut'],
                f['date'],
                f['hh'],
                f['duree'],
                f['module'],
                f['groupe'],
                f['pres'],
                f['prof'],
                f['fid'],
                f['excuse'])
            Database.remove(collection='fiche', query={ 'id' : f['id'] })
            new_fiche.save_to_mongo()

        return redirect(url_for('stats'))

@app.route('/ens/update', methods=['POST'])
def ens_update():
    if 'username' not in session:
        return redirect(url_for('index'))

    f = Database.find_one(collection='fiche',
            query={ 'id' : request.form['id']
                })
    new_fiche = Fiche(
                f['name'],
                f['nom'],
                f['prenom'],
                f['promo'],
                f['statut'],
                f['date'],
                f['hh'],
                f['duree'],
                f['module'],
                f['groupe'],
                int(request.form['pres']),
                f['prof'],
                f['fid'],
                request.form['excuse']
                )
    Database.remove(collection='fiche', query={ 'id' : f['id'] })
    new_fiche.save_to_mongo()
    return redirect(url_for('edit'))

@app.route('/ens/stats/<mod>')
def stats_mod(mod):
    if 'username' not in session:
        return redirect(url_for('index'))

    tab = []
    users = Database.find(collection='users',
            query={'statut' : 'etu'
                })
    for u in users :
        fiches = Database.find(collection='fiche',
            query={ 'module' : mod ,
                    'name' : u['name']
                })
        total = {}
        total['nom'] = u['nom']
        total['prenom'] = u['prenom']
        total['pres'] = 0
        total['duree'] = 0
        total['justif'] = 0
        total['percent'] = 0
        for f in fiches:
            total['excuse'] = f['excuse']
            if f['excuse'] == 'oui':
                #total['pres'] = f['duree'] + total['pres']
                total['pres'] = f['pres'] + total['pres']
                total['justif'] = (f['duree'] - f['pres']) + total['justif']
            else:
                total['pres'] = f['pres'] + total['pres']
            total['duree'] = f['duree'] + total['duree']
            total['percent'] = 100*((total['pres'] + total['justif']) / total['duree'])
        if total['duree'] > 0:
            tab.append(total)

    return render_template('show_stat.html', t=tab, s=session, i = len(tab) )




@app.route('/ens/fiche', methods=['POST', 'GET'])
def ens():
    if 'username' not in session:
        return redirect(url_for('index'))
    if request.method == 'GET':
        if session['statut'] == 'ens':
            return render_template('ens01.html')

    session['date'] = request.form['dd']
    session['hh'] = request.form['hh']
    session['duree'] = request.form['duree']
    session['nbduree'] = int(int(request.form['duree']) / 15)
    session['module'] = request.form['module']
    session['promo'] = request.form['promo']
    session['groupe'] = request.form['groupe']

    if "cm" in request.form['groupe']:
        students = Database.find(collection='users',
            query={
                'statut' : 'etu',
                'promo' : request.form['promo'],
                'cm' : request.form['groupe']
                })

    if "TD" in request.form['groupe']:
        students = Database.find(collection='users',
            query={
                'statut' : 'etu',
                'promo' : request.form['promo'],
                'td' : request.form['groupe']
                })

    if "TP" in request.form['groupe']:
        students = Database.find(collection='users',
            query={
                'statut' : 'etu',
                'promo' : request.form['promo'],
                'tp' : request.form['groupe']
                })

    return render_template('fiche.html', u=students, s=session )


@app.route('/save', methods=['POST', 'GET'])
def save():
    if 'username' not in session:
        return redirect(url_for('index'))
    if request.method == 'GET':
        if session['statut'] == 'ens':
            return render_template('ens01.html')

    fid = uuid.uuid4().hex
    for etu in request.form.getlist('name'):
        selected = len(request.form.getlist(etu))
        stu = Database.find_one(collection='users',query={'name' : etu})
        fiche = Fiche(
            stu['name'],
            stu['nom'],
            stu['prenom'],
            session['promo'],
            stu['statut'],
            session['date'],
            session['hh'],
            int(session['duree']),
            session['module'],
            session['groupe'],
            int(int(selected)*15),
            session['username'],
            fid,
            'non'
        )
        fiche.save_to_mongo()
    fiche_prof = Fiche(
            session['username'],
            '',
            '',
            session['promo'],
            session['statut'],
            session['date'],
            session['hh'],
            int(session['duree']),
            session['module'],
            session['groupe'],
            int(session['duree']),
            session['username'],
            fid,
            ''
        )
    fiche_prof.save_to_mongo()
    return redirect(url_for('menu'))



@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        user = User(request.form['username'],
                request.form['pass'],
                request.form['nom'],
                request.form['prenom'],
                request.form['tp'],
                request.form['td'],
                request.form['cm'],
                request.form['promo'],
                request.form['statut']
                )
        existing_user = Database.find_one(collection='users', query={'name' : request.form['username']})

        if existing_user is None:
            user.save_to_mongo()
            session['username'] = request.form['username']
            return redirect(url_for('index'))

        return 'Existing user'
    return render_template('register.html')
